/*
 * php.fcgi wrapper.
 *
 * The purpose of this file is to avoid requiring a shell in
 * the users chroot.
 *
 * To compile, define PHPBIN to point at the php-cgi executable.
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#if !defined(PHPBIN)
#error PHPBIN not defined
#endif

extern char **environ;
#define MAX_ENV_SIZE   256

void log_err(char *s)
{
  fprintf(stderr, "%s\n", s);
  exit(1);
}

int main(int argc, char **argv)
{
  char **envp, **newenv;
  char **ep, **nep;
  int envidx = 0, err;

  newenv = (char **)calloc(MAX_ENV_SIZE, sizeof(char *));
  if (newenv == NULL)
    log_err("out of memory");
  envp = environ;
  for (ep = envp, nep = newenv; *ep && envidx < (MAX_ENV_SIZE-2); ep++) {
    *nep++ = *ep;
    envidx++;
  }
  *nep++ = "PHP_FCGI_CHILDREN=0";
  *nep++ = "PHP_FCGI_MAX_REQUESTS=50000";
  *nep++ = NULL;
  environ = newenv;

  umask(002);

  err = execv(PHPBIN, argv);
  if (err < 0) {
    log_err(strerror(errno));
    return -errno;
  }
  return 0; /* unreached. */
}

